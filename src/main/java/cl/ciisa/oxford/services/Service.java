package cl.ciisa.oxford.services;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;

import cl.ciisa.oxford.model.entities.Definicion;
import cl.ciisa.oxford.model.api.Entry;
import cl.ciisa.oxford.model.api.LexicalEntry;
import cl.ciisa.oxford.model.api.OxfordDef;
import cl.ciisa.oxford.model.api.Result;
import cl.ciisa.oxford.model.api.Sense;
import cl.ciisa.oxford.model.dao.DefinicionDAO;
import cl.ciisa.oxford.model.dao.exceptions.FunctionalException;
import cl.ciisa.oxford.model.dao.exceptions.PreexistingEntityException;
import cl.ciisa.oxford.model.domain.HistorialResponse;
import cl.ciisa.oxford.model.domain.ResultResponse;
import java.util.ArrayList;

public class Service {

    private Gson gson = new Gson();
    private final Logger log = Logger.getLogger(this.getClass().getName());
    DefinicionDAO dao = new DefinicionDAO();
    ApiOxford api = new ApiOxford();

    public ResultResponse findWord(String word) throws PreexistingEntityException, Exception {
        log.info("Service::findWord::" + word);

        OxfordDef obj = api.getDefinitions(word);

        if (obj.results.size() == 0) {
            log.severe("Service::findWord::Word not found!!");
            throw new FunctionalException("404", "Palabra no existe en diccionario");
        }

        log.info("Service::getApiResult:findDefinitions");
        List<String> auxdef = new ArrayList<String>();
        for(Result r:obj.results){
            for(LexicalEntry l:r.lexicalEntries){
                for(Entry e:l.entries){
                    for(Sense s:e.senses){
                        log.info("Service::Definitions::Found Definition::" + s.id);
                        auxdef.addAll(s.definitions);
                    }
                }
            }
        }

        ResultResponse response = new ResultResponse();

        response.setPalabra(word);
        response.setDefiniciones(auxdef);

        log.info("Service::saveDB::Definicion");
        List<Definicion> resultSet = dao.findWord(word);
        if (resultSet.size()==0){
            Long id = dao.create(mapper(response)).getId();
            response.setId(id);
        } else {
            response.setId(resultSet.get(0).getId());
            dao.edit(mapper(response));
        }

        return response;
    }
    
    public HistorialResponse findHistorial(){
        List<ResultResponse> hist = new ArrayList<>();
        dao.findDefinicionEntities().stream().forEach(def -> {
            log.info("Procesando historial : " + def.getPalabra());
            ResultResponse res = new ResultResponse();
            res.setId(def.getId());
            res.setPalabra(def.getPalabra());
            res.setDefiniciones(gson.fromJson(def.getDefiniciones(), ArrayList.class));
            hist.add(res);
        });
        log.info("Fin proceso historial...");
        HistorialResponse response = new HistorialResponse();
        response.getHistorial().addAll(hist);
        return response;
    }

    public Definicion mapper(ResultResponse r){
        Definicion def = new Definicion();

        def.setId(r.getId());
        def.setPalabra(r.getPalabra());
        def.setDefiniciones(gson.toJson(r.getDefiniciones()));

        return def;
    }


}
