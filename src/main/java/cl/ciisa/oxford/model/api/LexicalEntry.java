
package cl.ciisa.oxford.model.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "entries",
    "language",
    "lexicalCategory",
    "text"
})
public class LexicalEntry {

    @JsonProperty("entries")
    public List<Entry> entries = new ArrayList<Entry>();
    @JsonProperty("language")
    public String language;
    @JsonProperty("lexicalCategory")
    public LexicalCategory lexicalCategory;
    @JsonProperty("text")
    public String text;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public LexicalEntry withEntries(List<Entry> entries) {
        this.entries = entries;
        return this;
    }

    public LexicalEntry withLanguage(String language) {
        this.language = language;
        return this;
    }

    public LexicalEntry withLexicalCategory(LexicalCategory lexicalCategory) {
        this.lexicalCategory = lexicalCategory;
        return this;
    }

    public LexicalEntry withText(String text) {
        this.text = text;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public LexicalEntry withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
