/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.oxford.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import cl.ciisa.oxford.model.entities.Definicion;
import cl.ciisa.oxford.model.dao.exceptions.PreexistingEntityException;
import cl.ciisa.oxford.model.dao.exceptions.NonexistentEntityException;

/**
 *
 * @author mono
 */
public class DefinicionDAO implements Serializable {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("db");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public DefinicionDAO(final EntityManagerFactory emf) {
        this.emf = emf;
    }

    public DefinicionDAO() {

    }

    public Definicion create(final Definicion definicion) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(definicion);
            em.getTransaction().commit();
        } catch (final Exception ex) {
            if (findDefinicion(definicion.getId()) != null) {
                throw new PreexistingEntityException("Definicion " + definicion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return definicion;
    }

    public Definicion edit(Definicion definicion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            definicion = em.merge(definicion);
            em.getTransaction().commit();
        } catch (final Exception ex) {
            final String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                final Long id = definicion.getId();
                if (findDefinicion(id) == null) {
                    throw new NonexistentEntityException("The definicion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return definicion;
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Definicion definicion;
            try {
                definicion = em.getReference(Definicion.class, id);
                definicion.getId();
            } catch (final EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The definicion with id " + id + " no longer exists.", enfe);
            }
            em.remove(definicion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Definicion> findDefinicionEntities() {
        return findDefinicionEntities(true, -1, -1);
    }

    public List<Definicion> findDefinicionEntities(final int maxResults, final int firstResult) {
        return findDefinicionEntities(false, maxResults, firstResult);
    }

    private List<Definicion> findDefinicionEntities(final boolean all, final int maxResults, final int firstResult) {
        final EntityManager em = getEntityManager();
        final CriteriaQuery cq = emf.getCriteriaBuilder().createQuery();
        final Root<Definicion> rt = cq.from(Definicion.class);
        try {
            final Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Definicion findDefinicion(Long id) {
        final EntityManager em = getEntityManager();
        try {
            return em.find(Definicion.class, id);
        } finally {
            em.close();
        }
    }

    public List<Definicion> findWord(String word){
        final EntityManager em = getEntityManager();
        try {
            final Query q = em.createQuery("SELECT p FROM Definicion p WHERE p.palabra = '" + word + "'", Definicion.class);
            return q.getResultList();
        } finally {
            em.close();
        }     
    }

    public int getDefinicionCount() {
        final EntityManager em = getEntityManager();
        final CriteriaQuery cq = emf.getCriteriaBuilder().createQuery();
        final Root<Definicion> rt = cq.from(Definicion.class);
        try {
            cq.select(em.getCriteriaBuilder().count(rt));
            final Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
